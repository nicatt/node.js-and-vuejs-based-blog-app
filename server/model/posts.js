'use strict';

const Connection = require('./database');

module.exports = class Posts extends Connection{
    constructor(){
        super();
    }

    async getFeed() {
        return await this.query("SELECT id, title, content FROM articles ORDER BY id DESC;", []);
    }

    async getPostByID(id) {
        let sql = "SELECT id, title, content, keywords, create_date FROM articles WHERE id=?;"

        return await this.query(sql, [id]);
    }
};
