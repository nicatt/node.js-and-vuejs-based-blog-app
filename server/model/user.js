'use strict';

const Connection = require('./database');

module.exports = class User extends Connection{
    constructor(){
        super();
    }

    async login(email, password) {
        const sha256 = require('sha256');

        let pass = sha256(sha256(password));
        let sql = "SELECT id, fullname, email FROM users WHERE email=? AND password=?;";

        return await this.query(sql, [email, pass]);
    }
};
