'use strict';

const mysql = require('mysql');
const escapeHtml = require('escape-html');

module.exports = class Connection {
    constructor() {
        this.pool = mysql.createPool({
            host: 'localhost',
            user: 'root',
            password: '',
            database: 'blog'
        });

        this.pool.getConnection((err, connection) => {
            if (err) {
                if (err.code === 'PROTOCOL_CONNECTION_LOST') {
                    console.error('Database connection was closed.')
                }

                if (err.code === 'ER_CON_COUNT_ERROR') {
                    console.error('Database has too many connections.')
                }

                if (err.code === 'ECONNREFUSED') {
                    console.error('Database connection was refused.')
                }
            }

            if (connection) connection.release();
        });
    }

    query(sql, args = [], escape = true){
        let self = this;

        if(escape) {
            for (let i = 0; i < args.length; i++) {
                args[i] = escapeHtml(args[i]);
            }
        }

        return new Promise((resolve, reject) => {
            self.pool.query(sql, args, (err, rows) => {

                if (err) reject(err);
                else resolve(rows);
            });
        });
    }
};