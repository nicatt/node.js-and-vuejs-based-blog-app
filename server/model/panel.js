'use strict';

const Connection = require('./database');

module.exports = class Panel extends Connection{
    constructor(){
        super();
    }

    async getCategories() {
        let sql = "SELECT id, name FROM categories;";

        return await this.query(sql, []);
    }

    /**
     * Bir başa req.body istifadə olunur deyə try-catch ilə yazılıb.
     * Post ilə bütün field-lar gəlməyə bilər.
     */
    async createPost(data) {
        const escape = require('escape-html');

        try {
            let title = escape(data.title);
            let selectedCategory = escape(data.selectedCategory);
            let keywords = escape(data.keywords.join(" "));

            let sql = `INSERT INTO articles (title, content, keywords, category_id)
                       VALUES(?, ?, ?, ?);`;

            return await this.query(sql, [title, data.content, keywords, selectedCategory], false);
        }
        catch (e) {
            return {
              status: 'failed'
            };
        }
    }

    /**
     * Bir başa req.body istifadə olunur deyə try-catch ilə yazılıb.
     * Post ilə bütün field-lar gəlməyə bilər.
     */
    async updatePost(id, data) {
        const escape = require('escape-html');

        try {
            let title = escape(data.title);
            let selectedCategory = escape(data.selectedCategory);
            let keywords = escape(data.keywords);

            let sql = `UPDATE articles SET 
                          title = ?, 
                          content = ?, 
                          keywords = ?, 
                          category_id = ?
                       WHERE articles.id = ?;`;

            let params = [title, data.content, keywords, selectedCategory, id];

            return await this.query(sql, params, false);
        }
        catch (e) {
            return {
                status: 'failed'
            };
        }
    }

    async deletePost(id) {
        let sql = `DELETE FROM articles WHERE id=?;`;

        return await this.query(sql, [id], false);
    }

    async getAllPosts() {
        let sql =
           `SELECT
              a.id, 
              a.title,
              a.content, 
              c.name AS category_name,
              DATE_FORMAT(a.create_date, "%Y-%m-%d") AS create_date
            FROM articles a
            JOIN categories c ON c.id=a.category_id
            ORDER BY a.id DESC;`;

        return await this.query(sql, []);
    }

    async getPost(id) {
        let sql =
            `SELECT
              a.id, 
              a.title,
              a.content,
              a.keywords,
              c.id AS category_id
            FROM articles a
            JOIN categories c ON c.id=a.category_id
            WHERE a.id=?;`;

        return await this.query(sql, [id]);
    }
};
