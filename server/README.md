# server

## Project setup
```
npm install
```

## Run project
```
node bin/www
```

Server will be run on localhost:3000

## On Production
Remove CORS settings on production in app.js file.