import Vue from 'vue';
import axios from 'axios';
import Vuetify from 'vuetify';
import App from './App.vue';
import router from './router';
import VueDisqus from 'vue-disqus';
import 'vuetify/dist/vuetify.min.css';

Vue.config.productionTip = false;
window.EventBus = new Vue();
window.axios = axios;
window.axios.defaults.withCredentials = true;
window.FULL_PATH = 'https://nicat.org';
window.API_URL = FULL_PATH+'/api';

Vue.use(Vuetify);
Vue.use(VueDisqus);

new Vue({
  router,
  render(h) { return h(App); },
}).$mount('#app');
